__author__ = 'Martin Fiser'
__credits__ = 'Martin Fiser, 2017, Twitter: @VFisa'

# Import Libraries
import os
import requests


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
print script_path



def send_complex_message(to_id, from_id, subject, html_body, attachments=None):
    '''
    to, from_id, subject, and html_body should be self explanatory.
    attachments is a list of file paths, like this:

    ['/tmp/tmp5paoks/image001.png','/tmp/tmp5paoks/test.txt']
    '''
    sandbox_url = "https://api.mailgun.net/v3/sandbox694d3337c2e84d2e952cc4df179ff6b7.mailgun.org/messages"
    live_url = "https://api.mailgun.net/v2/mydomain.com/messages"

    data={"from": from_id,
          "to": [to_id, ""],
          "subject": subject,
          "html": html_body}

    files = None      
    if attachments:
        files = {}
        count=0
        for attachment in attachments:
            with open(attachment, 'rb') as f:
                files['attachment['+str(count)+']'] = (os.path.basename(attachment), f.read())    
            count = count+1

    return requests.post(sandbox_url,
        auth=(USER, PASSWORD),
        files=files,
        data=data)


## test data
from_id = "Mailgun Sandbox <postmaster@sandbox694d3337c2e84d2e952cc4df179ff6b7.mailgun.org>"
to_id = "Fisa <fisa@keboola.com>"
subject = "Hello TEST"
html_body = "<html>HTML version of the body</html>"
tags = ["September newsletter", "newsletters"]
USER = "api"
PASSWORD = "key-21bfb3cdf3297fec910c2df4c2117a69"


send_status = send_complex_message(to_id, from_id, subject, html_body, attachments=None)
print send_status
