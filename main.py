__author__ = "Martin Fiser"
__credits__ = "Martin Fiser, 2017, Twitter: @VFisa"


# Import Libraries
import os
import csv
import sys
import requests
import logging
from keboola import docker


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(filename='python_job.log',
                    level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")


# Get proper list of tables
cfg = docker.Config('/data/')

# Source parameters
params = cfg.get_parameters()
name_from = cfg.get_parameters()["name_from"]
email_from = cfg.get_parameters()["email_from"]
user = cfg.get_parameters()["user"]
password = cfg.get_parameters()["password"]
specs = [name_from, email_from, user, password]
url = cfg.get_parameters()["mailgun_url"]
#print(specs)
# in production, put this into parameter window:
"""
(
    "name_from": "",
    "email_from": "",
    "user": "",
    "password": "",
    "mailgun_url": ""
    }
"""

# determine source data
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))



def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name


def input_file(file_in):
    """
    Read input data as list (without pandas)
    """

    try:
        with open(file_in, 'r', encoding='utf-8') as csvfile:
            file_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            input_data = list(file_reader)
        logging.info("Input file read.")
        print("Input file read.")
    except Exception as e:
        logging.error("Could not read input CSV file! Exit.")
        logging.error(str(e))
        print("Could not read input CSV file! Exit.")
        sys.exit(1)

    return input_data


def parse_input(input_data):
    """
    parse input table via dictionary
    """

    keys = input_data[0]
    values = input_data[1]

    parameters = dict(zip(keys, values))
    #print(parameters)

    email = parameters["email"]
    name = parameters["name"]
    text = parameters["text"]
    subject_line = parameters["subject"]

    return name, email, subject_line, text


def send_complex_message(to_id, from_id, user, password, subject, html_body, url, attachments=None):
    '''
    # Note: Attachment part can be finished later on...
    attachments is a list of file paths, like this:
    ['/tmp/tmp5paoks/image001.png','/tmp/tmp5paoks/test.txt']

    URL test
    sandbox_url = "https://api.mailgun.net/v3/sandbox694d3337c2e84d2e952cc4df179ff6b7.mailgun.org/messages"
    live_url = "https://api.mailgun.net/v2/mydomain.com/messages"
    '''

    # If no text available, alert
    if html_body=="":
        html_body = "No text to be send was available."
        logging.warning("No text to be send was available.")
        print("No text to be send was available.")
    else:
        pass

    data = {
        "from": from_id,
        "to": [to_id, ""],
        "subject": subject,
        "html": html_body
        }

    files = None
    if attachments:
        files = {}
        count = 0
        for attachment in attachments:
            with open(attachment, 'rb') as f:
                files['attachment['+str(count)+']'] = (os.path.basename(attachment), f.read())
            count = count+1

    response = requests.post(url,
                             auth=(user, password),
                             files=files,
                             data=data)

    print("Response: {0} ".format(response))
    print(response.headers)

    return response



if __name__ == "__main__":
    """
    """

    # Get data
    file_in = get_tables(in_tables)
    input_data = input_file(file_in)
    #print(input_data)

    # parse data to pre-defined variables
    name_to, email_to, subject, html_body = parse_input(input_data)

    # Assemble to and from fields
    to_id = (str(name_to)+" <"+str(email_to)+">")
    from_id = (str(name_from)+" <"+str(email_from)+">")
    print("Email to: {0} ".format(to_id))
    print("Email from: {0} ".format(from_id))

    # Send email
    send_status = send_complex_message(to_id, from_id, user, password, subject, html_body, url, attachments=None)

    logging.info("Script completed.")
